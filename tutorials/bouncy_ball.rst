.. metadata-placeholder

   :authors: - Mattia Basaglia

   :license: Creative Commons License SA 4.0


.. _bouncing_ball:


Bouncing Ball
=============

The bouncing ball animation is a common starting point for many animators, as it serves as a simple yet effective demonstration of motion and timing.

This tutorial will guide you through the process of creating a bouncing ball animation in Glaxnimate. So let's get started!

.. lottie::
   :json: /files/ball-final.json
   :rawr: /files/ball-final.rawr
   :height: 256px
   :width: 256px


Create a new project
--------------------

When you start Glaxnimate a dialog with default templates shows up, here select Telegram Sticker and ensure the framerate is 60 fps and the duration is 2 seconds.

.. figure:: /images/tutorials/template.png
  :align: left
  :width: 250px
  :alt: Startup Dialog

  Startup Dialog

.. rst-class:: clear-both

If you don't see the dialog, you can create a new file,
then change the timing in :menuselection:`Document --> Timing` as follows:

.. figure:: /images/tutorials/timing.png
  :align: left
  :width: 250px
  :alt: Timing Dialog

  Timing Dialog


Creating the Ball
-----------------

.. figure:: /images/tutorials/tool.png
  :align: left
  :width: 250px
  :alt: Ellipse Tool

  Ellipse Tool

Draw a circle shape to represent the ball. You can do this by selecting the "Ellipse" tool from the toolbar, then clicking and dragging the cursor to create a circle of the desired size. If you hold :kbd:`Ctrl`, Glaxnimate will ensure the ellipse is a perfect circle.

.. rst-class:: clear-both

.. figure:: /images/tutorials/select.png
  :align: left
  :width: 250px
  :alt: Selected Ellipse

  Selected Ellipse

You can then switch to the edit tool and ensure the ellipse to is selected.


Basic Animation Loop
--------------------

On the timeline view below the canvas, you can expand the tree to view the properties of an ellipse.

.. figure:: /images/tutorials/timeline-empty.png
  :align: left
  :width: 250px
  :alt: Properties in the Timeline

  Properties in the Timeline

To add the first keyframe, right click on the **position** row under the ellipse shape and select :menuselection:`Add Keyframe` from the context menu.

Then move the timeline to frame 60 and drag the ellipse down (or edit the position value on the timeline) and add another keyframe.

Finally, right click on the position row again and make it a looping animation. This last step copies the value of the first keyframe at the end of the animation.

.. figure:: /images/tutorials/timeline-full.png
  :align: left
  :width: 250px
  :alt: Keyframed Animation

  Keyframed Animation

If you play the animation back, you'll see the ball moving up and down like so:

.. lottie::
   :json: /files/ball-stiff.json
   :rawr: /files/ball-stiff.rawr
   :height: 256px
   :width: 256px


It works but the animation feels a bit stiff, because the ball is moving at a constant speed.


Easing
------

You could add more keyframes to represent the acceleration of the ball but there's an easier way: changing the easing curve of the keyframe.

Right click on the first keyframe in the timeline and change :guilabel:`Transition to Next` to :guilabel:`Ease`. Similarly, select the last keyframe and set the :guilabel:`Transition from Previous` also to :guilabel:`Ease`.

.. figure:: /images/tutorials/timeline-ease.png
  :align: left
  :width: 250px
  :alt: Timeline with Easing

  Timeline with Easing

What this does is make the ball speed up as it leaves the first keyframe and slow down to reach the last.

.. lottie::
   :json: /files/ball-smooth.json
   :rawr: /files/ball-smooth.rawr
   :height: 256px
   :width: 256px



To make the effect more pronounced, we can use custom easing curves.

Follow the procedure as above, but select "Custom", then edit so the keyframes easing curves look like these:


.. figure:: /images/tutorials/easing-custom-1.png
  :align: left
  :width: 250px
  :alt: Easing of the first keyframe

  Easing of the first keyframe

.. figure:: /images/tutorials/easing-custom-2.png
  :align: left
  :width: 250px
  :alt: Easing of the last keyframe

  Easing of the last keyframe

.. lottie::
   :json: /files/ball-extra-smooth.json
   :rawr: /files/ball-extra-smooth.rawr
   :height: 256px
   :width: 256px



Squishing
---------

Currently the ball is not being deformed by the bounce, to make the animation even better, we should change the size of the ellipse as it hits the bottom.

Setting all the keyframes manually is a bit tedious so we can enable the keyframe recording mode, which automatically adds a keyframe whenever we make a change.


.. figure:: /images/tutorials/record.png
  :align: left
  :width: 250px
  :alt: Keyframe Recording Mode

  Keyframe Recording Mode

Now select a frame before the ball reaches the bottom of the canvas, right click on the size and add a new keyframe, then select the time at which the ball is at its lower point (it should be frame 60) and change the size of the ball to make it wider and shorter, then after a few frames, make the ball tall and skinny.

Finally, loop the animation as we did with the position and drag the last keyframe towards the other.

In the end you should have something along these lines:

.. figure:: /images/tutorials/squish-1.png
  :align: left
  :width: 250px
  :alt: Squishing

  Squishing

.. figure:: /images/tutorials/squish-2.png
  :align: left
  :width: 250px
  :alt: Stretching

  Stretching


.. lottie::
   :json: /files/ball-squish.json
   :rawr: /files/ball-squish.rawr
   :height: 256px
   :width: 256px



Multiple Bounces
----------------

Currently the animation consists of a single bounce of the ball, which is fine for a simple example but sometimes you want to repeat an action or element within the animation.

In order to achieve this, we will look at Precompositions, a feature that allow you to group and organize objects into a single container, making it easier to manage and duplicate your animation elements.

First, let's start by turning our animation layer into a precomposition: Right click on **Layer** in the timeline, and select :guilabel:`Precompose` from the context menu.

.. figure:: /images/tutorials/precomposed.png
  :align: left
  :width: 250px
  :alt: Precomposed Layer

  Precomposed Layer

You'll notice now on top of the canvas you have two tabs, each tab represents a composition. The first composition corresponds to the final animation, and all the others are precompositions you can insert in the main animation or even in other precompositions.

If you click on the **Animation** tab, you'll see the old shape layer has been replaced by a composition layer.

The animation looks the same but now the layer behaves as a single uneditable object.

.. figure:: /images/tutorials/precomposed-timeline.png
  :align: left
  :width: 250px
  :alt: Timeline showing precomposed layers

  Timeline showing precomposed layers

Now let's make the ball bounce twice as fast, in the main composition you can drag the end time of the precomposed layer to end at frame 60.

.. rst-class:: clear-both

.. figure:: /images/tutorials/timeline-stretch.png
  :align: left
  :width: 250px
  :alt: Timeline showing the stretched layer time

  Timeline showing the stretched layer time

If you want more control, ensure you have the composition layer selected and set the timing stretch to 50%:

.. figure:: /images/tutorials/properties-stretch.png
  :align: left
  :width: 250px
  :alt: Property tree showing the stretched layer time

  Property tree showing the stretched layer time

Now you can right click the composition layer in the timeline and select :guilabel:`Duplicate`. Finally, drag the start time of the duplicated layer to frame 60, in a similar manner as to which we modified the time stretch.

.. figure:: /images/tutorials/second-precomp.png
  :align: left
  :width: 250px
  :alt: Timeline with the two precomp layers

  Timeline with the two precomp layers

Now you have the ball bounce twice per animation loop:

.. lottie::
   :json: /files/ball-twice.json
   :rawr: /files/ball-twice.rawr
   :height: 256px
   :width: 256px



Hit Lines
---------

We will now add some lines to highlight the hit when the ball reaches the bottom.

Within the precomposition (**Layer** tab), go to frame 60 (when the ball is at its lowest) and use the :guilabel:`Draw Bezier` tool to draw a line from the bottom of the ball out. Make sure that in the tool options :guilabel:`Fill` is unchecked and :guilabel:`Stroke` is checked. Simply click under the ball with the :guilabel:`Draw Bezier` tool active and then click again on the end of the line. Then press Enter on your keyboard to confirm the line.

.. figure:: /images/tutorials/draw-line.png
  :align: left
  :width: 250px
  :alt: Drawing a line

  Drawing a line

You can then use the :guilabel:`Stroke` view to change the color and thickness of the line.

.. rst-class:: clear-both

.. figure:: /images/tutorials/line-style.png
  :align: left
  :width: 250px
  :alt: Styled line

  Styled line

Draw a few more lines going in different directions making sure the staring point is towards the ball, then select them all (with the :guilabel:`Select` tool or from the timeline) and press Ctrl+G to group them. Move the group below the ball by clicking on the :guilabel:`Lower to Bottom` button on the toolbar above the canvas.

.. rst-class:: clear-both

.. figure:: /images/tutorials/multiple-lines.png
  :align: left
  :width: 250px
  :alt: Multiple Lines

  Multiple Lines

Now we'll animate the lines, making them appear and disappear in rapid succession.

Right click on the line group in the timeline, then :menuselection:`Add --> Trim Path`. This will add a Trim Path at the bottom of the group, which gives you some properties you can animate to modify the other paths in the group.

Expand **Trim Path** on the timeline, then animate its properties as we did with the ball. At frame 60, both :guilabel:`start` and :guilabel:`end` should be keyframed at 0%, this will make the lines invisible until that point. After a few frames add set :guilabel:`start` to 50% and :guilabel:`end` to 100% and add a new keyframe for both. After a few more frames add another keyframe and setting :guilabel:`start` to 100%.

All this will make the lines briefly appear and shoot out when the ball hits the bottom.

.. rst-class:: clear-both

.. figure:: /images/tutorials/timeline-trim-path.png
  :align: left
  :width: 250px
  :alt: Trim Path keyframes

  Trim Path keyframes

.. lottie::
   :json: /files/ball-lines.json
   :rawr: /files/ball-lines.rawr
   :height: 256px
   :width: 256px

