.. metadata-placeholder

   :authors: - Mattia Basaglia

   :license: Creative Commons License SA 4.0


.. _dockable_views:

Dockable Views
==============

Tools
-----

.. figure:: /images/views/tools.png
   :align: left
   :width: 250px
   :alt: Tools View

   Tools View

This view allows you to select the active :ref:`tool<tools>`.

See :ref:`tools` for details on how each tool works.


Tool Options
------------

.. figure:: /images/tools/shape_options.png
   :align: left
   :width: 250px
   :alt: Shape Options

   Shape Options

This view shows extra option for the currently active :ref:`tool<tools>`.

See :ref:`tools` for details on how each tool works.

.. _fill_dock:

Fill
----

.. figure:: /images/colors/hsv.png
   :align: left
   :width: 250px
   :alt: Colors View

   Colors View

This view allows to select / modify the current color.

The top part has different tabs to change which color space to use (or to use palettes).

The bottom part stays the same:

.. figure:: /images/colors/bottom.png
   :align: left
   :width: 250px
   :alt: Colors View Bottom

   Colors View Bottom

The "A" slider changes the color opacity (Alpha).

The two color indicators show the main (fill) color and secondary (stroke) color.

Clicking on either of them shows a dialog which gives similar controls as the ones in the view, but more compact. And also allows picking colors from the screen.

The :guilabel:`Swap` button |swap-panels| swaps the two colors.

Finally, there's a textbox with a hexadecimal representation of the main color.

HSV
~~~

.. figure:: /images/colors/hsv.png
   :align: left
   :width: 250px
   :alt: HSV View

   HSV View

Colors selectors in the HSV space (Hue Saturation Value).

The color can be adjusted with the color wheel or individual channel sliders.

HSL
~~~

.. figure:: /images/colors/hsl.png
   :align: left
   :width: 250px
   :alt: HSL View

   HSL View

Colors selectors in the HSL space (Hue Saturation Lightness).

The color can be adjusted with the color wheel or individual channel sliders.

RGB
~~~

.. figure:: /images/colors/rgb.png
   :align: left
   :width: 250px
   :alt: RGB View

   RGB View

Colors selectors in the RGB space (Red Green Blue).

The color can be adjusted with the individual channel sliders.

CMYK
~~~~

.. figure:: /images/colors/cmyk.png
   :align: left
   :width: 250px
   :alt: CMYK View

   CMYK View

Colors selectors in the CMYK space (Cyan Magenta Yellow Black).

The color can be adjusted with the individual channel sliders.

Palette
~~~~~~~

.. figure:: /images/colors/palette.png
   :align: left
   :width: 250px
   :alt: Palette View

   Palette View

Here you can select, create, modify color palettes.

Opening and saving supports Gimp Palette files (.gpl).

Clicking on one of the colors, selects it as the current color.

.. _stroke_dock:

Stroke
------

This view shows various stroke (outline) settings.

Stroke Style
~~~~~~~~~~~~

.. figure:: /images/stroke/style.png
   :align: left
   :width: 250px
   :alt: Stroke Style Dock

   Stroke Style Dock

At the bottom it shows a preview of the current settings.

The "Width" spin box determines how thick the stroke is.

The "Cap" buttons determine the style of the ends of the line.

The "Join" buttons determine the style of sharp corners.

The spin box after the "Join" buttons determines how far a Miter join can reach.

Stroke Color
~~~~~~~~~~~~

.. figure:: /images/stroke/color.png
   :align: left
   :width: 250px
   :alt: Stroke Style Dock

   Stroke Style Dock

This tab selects the stroke color in a similar way as to how the :ref:`Fill View<fill_dock>`
selects the fill color.


Layers
------

.. figure:: /images/views/layers.png
   :align: left
   :width: 250px
   :alt: Stroke Style Dock

   Stroke Style Dock

This view shows layers and shapes.

From here you can rename them by double clicking on their name. You can hide and lock them by clicking on the eye and padlock icons respectively.

You can also select a grouping color by clicking on the rectangle to thee left. This has no effect other than changing how layers are shown in this view to help organize them.

Context Menu
~~~~~~~~~~~~

.. figure:: /images/tools/shape_menu.png
   :align: left
   :width: 250px
   :alt: Context Menu

   Context Menu

Right clicking on an item will bring a context menu with quick actions for that item.


Timeline
--------

.. figure:: /images/views/timeline/timeline.png
   :align: left
   :width: 250px
   :alt: Timeline Dock

   Timeline Dock

The timeline view allows to manage keyframes for the animatable properties of the active object.

Playback Buttons
~~~~~~~~~~~~~~~~

.. figure:: /images/views/timeline/buttons.png
   :align: left
   :width: 250px
   :alt: Playback Buttons

   Playback Buttons

At the top of the Timeline view shows the current frame, and various playback buttons.

The frame spin box shows the current frame, and allows jumping to a specific frame by typing its number.

|media-playback-start| Starts and pauses playback

|media-playlist-repeat| Toggles whether playback loops the animation

|go-first| Jumps to the first frame

|go-previous| Jumps to the previous frame

|go-next| Jumps to the next frame

|go-last| Jumps to the last frame

|media-record| Record keyframes. When enabled, all changes made on the canvas are added as keyframes.

Composition Tabs
~~~~~~~~~~~~~~~~

.. figure:: /images/views/timeline/composition_tabs.png
   :align: left
   :width: 250px
   :alt: Composition Tabs

   Composition Tabs

If you have multiple compositions in the current document, they are shown in a tab bar above the property list and timeline.

Clicking on the tabs will switch to a different composition.

Property and Object List
~~~~~~~~~~~~~~~~~~~~~~~~

.. figure:: /images/views/timeline/property_list.png
   :align: left
   :width: 250px
   :alt: Property List

   Property List

On its left handside, the Timeline view has the list of objects and properties.

For shapes and other objects, it shows their group color, visibility, lock status, and name. All of those can be modified from the timeline itself.

Additionally, top-level layers can also have a parent, which is also shown and edited from the timeline.

Nested inside each object, you can see their children and properties.

Properties show their name, current value, and animation status.


The animation statuses are as follow:

|keyframe-not-animated| Not animated

|keyframe-key| Currently on a keyframe

|keyframe-tween| "Tweening" value, between keyframes

|keyframe-mismatch| Animated value that has been moved without adding a keyframe

Keyframe Timeline
~~~~~~~~~~~~~~~~~

The right handside of the Timeline view shows the actual timeline.


Pressing left/right arrow keys changes the current frame when this area is focused.


Time Bar
********

.. figure:: /images/views/timeline/time_bar.png
   :align: left
   :width: 250px
   :alt: Time Bar

   Time Bar

The bar at the top highlights the current frame.
Clicking or dragging with the mouse will jump to the frame whose number is under the mouse cursor.

Keyframe Area
*************

.. figure:: /images/views/timeline/keyframe_area.png
   :align: left
   :width: 250px
   :alt: Keyframe Area

   Keyframe Area

Under that, there are rows for each of the animatable properties, and icons showing the keyframes for said properties.

The keyframes icons can be selected by clicking (To select multiple :kbd:`Shift+Click` or :kbd:`Ctrl+Click`). and dragged with the mouse to change their time.

The keyframe icons show different shapes and colors depending on the easing transition of the keyframe before and after the icon.

|keyframe-linear| Linear, values change from the start to the end of the keyframe at a constant rate

|keyframe-ease| Smooth Easing, values change more slowly at the start or end of the keyframe

|keyframe-hold| Hold, the value remains constant until the next keyframe

|keyframe-custom| Custom curve, defined with the dialog described in [Custom Easing Dialog](#custom-easing-dialog)

:kbd:`Ctrl+Mouse Wheel` will zoom the area with respect to time, :kbd:`Mouse Wheel` by itself moves left or right. To scroll up and down either use the scrollbar to the side or use the :kbd:`Mouse Wheel` on the property list.

Layers will be displayed as rectangles that spans between the first and last frames of that layer:

.. figure:: /images/views/timeline/layers.png
   :align: left
   :width: 250px
   :alt: Keyframe Area with Layers

   Keyframe Area with Layers

Context Menus
*************

Right clicking on the row of a property, will show a context menu with the name of the property and the ability to add a keyframe:

.. figure:: /images/views/timeline/property_menu.png
   :align: left
   :width: 250px
   :alt: Property Menu

   Property Menu

Clicking on "Add Keyframe" will add a keyframe at the current frame with the current value of the property.

Right clicking on an existing keyframe icon, will show a menu with options relating to that keyframe:

.. figure:: /images/views/timeline/keyframe_menu.png
   :align: left
   :width: 250px
   :alt: Keyframe Menu

   Keyframe Menu

Clicking on "Custom..." for the incoming or outgoing transitions will show the [Custom Easing Dialog](#custom-easing-dialog).

Custom Easing Dialog
~~~~~~~~~~~~~~~~~~~~

.. figure:: /images/views/timeline/custom_easing.png
   :align: left
   :width: 250px
   :alt: Custom Easing Dialog

   Custom Easing Dialog

This dialog gives full freedom to specify easing curves.

The curve represents how the value changes between two keyframes:
the horizontal axis represents the time between the two keyframes
and the vertical axis is the interpolation factor between the two values.

To choose a preset transition for one of the two ends, you can select a value from the dropdowns.


Align
-----

.. figure:: /images/views/align.png
   :align: left
   :width: 250px
   :alt: Align View

   Align View

This view shows alignment options, the same actions are available in the [Align Menu](menus.md#align).

The dropdown selects whether to align based on the selection bounding box or the canvas,
the buttons are the various alignment options.


Properties
----------

.. figure:: /images/views/properties.png
   :align: left
   :width: 250px
   :alt: Property View

   Property View

The Properties view allows the user to view or modify any property of the active object.

To edit a property double click on its value.

Animatable properties are highlighted as explained in [Property List](#property-list).


Undo History
------------

.. figure:: /images/views/undo.png
   :align: left
   :width: 250px
   :alt: Undo History View

   Undo History View

This view allows you to see the action history for the current document,
clicking on the listed actions will revert the document to that action.

Useful for when you want to undo or redo several actions at once.

A little icon will be shown next to the action corresponding to the last time you saved the document.


Gradients
---------

.. figure:: /images/views/gradients/gradients_view.png
   :align: left
   :width: 250px
   :alt: Gradients View

   Gradients View

This view shows the gradients used in the document.

It's hidden by default, to show it use the :menuselection:`View --> Views --> Gradients` menu action.

At the top of the view is the list of gradients, showing a preview, its name,
and the number of shapes using it.

Below that, you have a few buttons:

* |list-add| Adds a new gradient based on the current colors.
* |folder| Adds a new gradients from a list of presets.
* |list-remove| Removes the selected gradient.

Then buttons to apply the selected gradient for fill or stroke:

|paint-gradient-linear| for a linear gradient.

|paint-gradient-radial| for a radial gradient.

Click on the checked button to remove the gradient from an object.

Presets
~~~~~~~

.. figure:: /images/views/gradients/presets.png
   :align: left
   :width: 250px
   :alt: Gradients Presets

   Gradients Presets

This menu shows a drop down with the available presets, based on https://webgradients.com/.

Editing from the List
~~~~~~~~~~~~~~~~~~~~~

You can rename gradients by double-clicking on their name and typing a new name.

Similarly, you can double click on the preview to make it editable:

.. figure:: /images/views/gradients/gradients_view.png
   :align: left
   :width: 250px
   :alt: Gradients View

   Gradients View

While in this mode, you can drag the lines representing the gradient stops to move them,
drag them off the rectangle to remove them, and double click to show a dialog to set the color.

Right-clicking shows this menu:

.. figure:: /images/views/gradients/edit_context_menu.png
   :align: left
   :width: 250px
   :alt: Gradients View

   Gradients View


Editing from the Canvas
~~~~~~~~~~~~~~~~~~~~~~~

With the [edit tool](tools.md#edit-tool) active, the gradient controls will appear on the canvas.

Dragging the end points will change the extent of the gradient and dragging the smaller handles
will change the offset of the corresponding gradient stop.

For a Linear gradient:

.. figure:: /images/views/gradients/linear.png
   :align: left
   :width: 250px
   :alt: Linear Gradient

   Linear Gradient

For a radial gradient:

.. figure:: /images/views/gradients/radial.png
   :align: left
   :width: 250px
   :alt: Radial Gradient

   Radial Gradient

When you have a radial gradient, you can shift click on the start handle to reveal
a new X-shaped handle that controls the highlight position for the gradient:

.. figure:: /images/views/gradients/radial_highlight.png
   :align: left
   :width: 250px
   :alt: Radial Highlight

   Radial Highlight

To hide it again, shift-click on it.


Swatch
------

.. figure:: /images/views/swatch/swatch.png
   :align: left
   :width: 250px
   :alt: Swatch View

   Swatch View

This view shows the colors in the document swatch.

The document swatch is a palette specific to the open document. When an object is assigned a color from the document swatch, it gets linked to it. Modifying the color in the swatch will reflect the change in all linked objects, so you can recolor multiple objects at once.

The view shows the palette for the document swatch, which each color in its own rectangle. There is an extra rectangle that's used to unlink the selected object from the swatch (the object will retain its color but modifying the swatch will no
longer affect that object).

These are the buttons at the bottom:


|open-menu-symbolic| Clicking and holding on this button will show [a menu with extra options](#swatch-extra-options).

|list-add| Adds the current fill color to the swatch. If an object is selected, its fill color will be linked to the swatch.

|list-remove| Removes the last clicked color from the swatch

Swatch Extra Options
~~~~~~~~~~~~~~~~~~~~

.. figure:: /images/views/swatch/extra_menu.png
   :align: left
   :width: 250px
   :alt: Menu Extra

   Menu Extra

|document-export| :guilabel:`Generate` Pulls the colors off the open document and link all objects to the swatch

|document-open| :guilabel:`From Palette...` Shows a [dialog](#from-palette) with option to populate the swatch from an existing palette.

|document-save| :guilabel:`Save Palette` Saves the swatch and it will show up as a [palette in the Fill and Stroke views](#palette).

Mouse interactions
~~~~~~~~~~~~~~~~~~

Left-clicking on one of the rectangles of the swatch will link the fill color of the selected object (or unlink if you click the rectangle marked with an X).

Holding Shift when you click will affect the stroke color instead of the fill color.

Right clicking will show the following context menu:

.. figure:: /images/views/swatch/context_menu.png
   :align: left
   :width: 250px
   :alt: Context Menu

   Context Menu

At the top of the menu you see the name of the color

|edit-rename| :guilabel:`Rename...`
Shows a dialog where you can change the name of the color.

|color-management| :guilabel:`Edit Color...`
Shows a dialog where you can change the color

|list-remove| :guilabel:`Remove`
Will remove the color from the swatch.
This will have no visual changes, but all objects previously linked to this
color will be unlinked.

|edit-duplicate| :guilabel:`Duplicate`
Will add a new identical color to the swatch.

|list-remove| :guilabel:`Set as fill`
Does the same as clicking

|format-fill-color| :guilabel:`Set as stroke`
Does the same as shift-clicking

|format-stroke-color| :guilabel:`Link shapes with matching colors`
Searches the document for all shapes with the same color as the one in the rectangle you clicked, and links them to the swatch.

From Palette
~~~~~~~~~~~~

.. figure:: /images/views/swatch/from_palette.png
   :align: left
   :width: 250px
   :alt: From Palette Dialog

   From Palette Dialog

At the top it shows a dropdown with the palettes currently loaded by glaxnimate. The selected item will define the colors used by the document swatch.

To add more palettes, you can do so in the [Palette section of the fill View](#palette).

* :guilabel:`Overwrite on save` means that clicking :guilabel:`Save Palette` for the document swatch
  will overwrite the selected palette.
* :guilabel:`Link shapes matching colors` When checked, Glaxnimate will scan the document
  for objects with colors in the palette and link them to the swatch.
* :guilabel:`Remove existing colors` If checked, any existing colors in the document swatch
  will be removed before adding new ones from the palette.
  Note that this means all objects will become unlinked.


Assets
------

.. figure:: /images/views/assets.png
   :align: left
   :width: 250px
   :alt: Assets View

   Assets View

This view shows a list of various assets used by the current document.


Script Console
--------------

.. figure:: /images/views/console.png
   :align: left
   :width: 250px
   :alt: Script Console

   Script Console

This view (hidden by default) allows you to inspect and modify the open document with Python.

See [Scripting](/contributing/scripting/index.md) for details.


Snippets
--------

.. figure:: /images/views/snippets.png
   :align: left
   :width: 250px
   :alt: Snippets

   Snippets

Here you can see "snippets", small python files you can quickly edit and execute.

Useful for testing new scripts when making plugins and to automate certain tasks without having to provide all the metadata required by plugins.

Editing a snippet will open your system text editor.

When you run a snippet, it's equivalent to running that code in the console.


Logs
----

.. figure:: /images/views/log.png
   :align: left
   :width: 250px
   :alt: Log View

   Log View

This view (hidden by default) shows internal reporting, errors, etc.
