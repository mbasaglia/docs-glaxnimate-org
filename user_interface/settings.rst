.. metadata-placeholder

   :authors: - Mattia Basaglia

   :license: Creative Commons License SA 4.0

.. _settings:

Settings Dialog
===============

User Interface
--------------


.. figure:: /images/dialogs/settings/ui.png
   :align: left
   :width: 250px
   :alt: settings dialog

   settings dialog

* Language: Changes the interface language
* Icon Theme: Changes the icon theme, it lists all the detected system themes
  plus :guilabel:`Default`. With :guilabel:`Default` Glaxnimate will select the icons based on
  the widget theme (ie: if you have a dark theme it will show bright icons).


New Animation Defaults
----------------------

.. figure:: /images/dialogs/settings/defaults.png
   :align: left
   :width: 250px
   :alt: settings dialog

   settings dialog

This select the defaults for when you create a new file.


Open Save
---------

.. figure:: /images/dialogs/settings/open_save.png
   :align: left
   :width: 250px
   :alt: settings dialog

   settings dialog

* Max Recent Files: How many items to show in the :menuselection:`File --> Open Recent` menu.
* Backup Frequency: Number of minutes between automatic backup saves. Set to 0 to disable.


Toolbars
--------

.. figure:: /images/dialogs/settings/toolbars.png
   :align: left
   :width: 250px
   :alt: settings dialog

   settings dialog

Allows the user to change the toolbar appearance.

Plugins
-------

.. figure:: /images/dialogs/settings/plugins.png
   :align: left
   :width: 250px
   :alt: settings dialog

   settings dialog

Here it shows all the available plugins and you can enable or disable them.

Clipboard
---------

.. figure:: /images/dialogs/settings/clipboard.png
   :align: left
   :width: 250px
   :alt: settings dialog

   settings dialog

Here you can select what is going to be supported when copying / pasting shapes.

* Glaxnimate Animation: Always active, to copy within Glaxnimate.
* SVG: Copy/Paste SVG, works well with [Inkscape](https://inkscape.org/)
* Raster Image: Copy only, the selection will be rendered as an image you can
  paste on image editing software.
* JSON: Copy only, the selection will be copied as text, with the contents as if
  saved as a Glaxnimate file.

Widget Theme
------------

.. figure:: /images/dialogs/settings/theme.png
   :align: left
   :width: 250px
   :alt: settings dialog

   settings dialog

Here you can select the interface theme colors.

You can select a preset, or create your own style.

Keyboard Shortcuts
------------------

.. figure:: /images/dialogs/settings/keyboard.png
   :align: left
   :width: 250px
   :alt: settings dialog

   settings dialog

Here you can assign keyboard shortcuts for the various menu actions.
