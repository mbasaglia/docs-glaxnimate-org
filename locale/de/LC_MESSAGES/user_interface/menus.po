# German translations for Glaxnimate Manual package
# German translation for Glaxnimate Manual.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Glaxnimate Manual package.
# Automatically generated, 2024.
#
msgid ""
msgstr ""
"Project-Id-Version: Glaxnimate Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-04-30 01:45+0000\n"
"PO-Revision-Date: 2024-04-30 08:46+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../../user_interface/menus.rst:11
msgid "Menus"
msgstr ""

#: ../../user_interface/menus.rst:14
msgid "File"
msgstr ""

#: ../../user_interface/menus.rst:20
msgid "The usual File menu actions."
msgstr ""

#: ../../user_interface/menus.rst:23
msgid "Edit"
msgstr ""

#: ../../user_interface/menus.rst:30 ../../user_interface/menus.rst:159
msgid "Tools"
msgstr ""

#: ../../user_interface/menus.rst:36
msgid "This menu toggles which is the active :ref:`tools`."
msgstr ""

#: ../../user_interface/menus.rst:39
msgid "View"
msgstr ""

#: ../../user_interface/menus.rst:46
msgid "Views"
msgstr ""

#: ../../user_interface/menus.rst:52
msgid "This menu toggles which :ref:`dockable_views` are visible."
msgstr ""

#: ../../user_interface/menus.rst:56
msgid "Toolbar Menu"
msgstr ""

#: ../../user_interface/menus.rst:62
msgid "This menu toggles which :ref:`toolbars` are visible."
msgstr ""

#: ../../user_interface/menus.rst:66
msgid "Document"
msgstr ""

#: ../../user_interface/menus.rst:73
msgid "Single frame"
msgstr ""

#: ../../user_interface/menus.rst:80
msgid "Web preview"
msgstr ""

#: ../../user_interface/menus.rst:86
msgid ""
"The actions in this menu will open your browser to an html page that embeds "
"the animation using the exporting options associated with said action."
msgstr ""

#: ../../user_interface/menus.rst:89
msgid "Playback"
msgstr ""

#: ../../user_interface/menus.rst:96
msgid "Layers"
msgstr ""

#: ../../user_interface/menus.rst:103
msgid "New layer"
msgstr ""

#: ../../user_interface/menus.rst:110
msgid "Object"
msgstr ""

#: ../../user_interface/menus.rst:117
msgid "Align"
msgstr ""

#: ../../user_interface/menus.rst:124
msgid "Path"
msgstr ""

#: ../../user_interface/menus.rst:131
msgid "Help"
msgstr ""

#: ../../user_interface/menus.rst:140
msgid "Toolbars"
msgstr ""

#: ../../user_interface/menus.rst:143
msgid "Main"
msgstr ""

#: ../../user_interface/menus.rst:150
msgid "Node Editing"
msgstr ""

#: ../../user_interface/menus.rst:156
msgid "This toolbar is only present when you have the edit tool active."
msgstr ""

#: ../../user_interface/menus.rst:168
msgid "Status Bar"
msgstr ""

#: ../../user_interface/menus.rst:174
msgid ""
"The status bar shows you whether you are in recording mode, the mouse "
"position, the current fill/stroke colors, and various widgets to adjust the "
"canvas transform."
msgstr ""
