# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Glaxnimate Manual package.
# SPDX-FileCopyrightText: 2024 Vincenzo Reale <smart2128vr@gmail.com>
#
msgid ""
msgstr ""
"Project-Id-Version: Glaxnimate Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-05-01 01:42+0000\n"
"PO-Revision-Date: 2024-05-31 11:40+0200\n"
"Last-Translator: \n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 24.05.0\n"

#: ../../user_interface/docks.rst:11
msgid "Dockable Views"
msgstr ""

#: ../../user_interface/docks.rst:14
msgid "Tools"
msgstr "Strumenti"

#: ../../user_interface/docks.rst:21
msgid "Tools View"
msgstr "Vista degli strumenti"

#: ../../user_interface/docks.rst:23
msgid "This view allows you to select the active :ref:`tool<tools>`."
msgstr ""

#: ../../user_interface/docks.rst:25 ../../user_interface/docks.rst:40
msgid "See :ref:`tools` for details on how each tool works."
msgstr ""

#: ../../user_interface/docks.rst:29
msgid "Tool Options"
msgstr ""

#: ../../user_interface/docks.rst:36
msgid "Shape Options"
msgstr "Opzioni della forma"

#: ../../user_interface/docks.rst:38
msgid ""
"This view shows extra option for the currently active :ref:`tool<tools>`."
msgstr ""

#: ../../user_interface/docks.rst:45
msgid "Fill"
msgstr "Riempi"

#: ../../user_interface/docks.rst:52
msgid "Colors View"
msgstr ""

#: ../../user_interface/docks.rst:54
msgid "This view allows to select / modify the current color."
msgstr ""

#: ../../user_interface/docks.rst:56
msgid ""
"The top part has different tabs to change which color space to use (or to "
"use palettes)."
msgstr ""

#: ../../user_interface/docks.rst:58
msgid "The bottom part stays the same:"
msgstr ""

#: ../../user_interface/docks.rst:65
msgid "Colors View Bottom"
msgstr ""

#: ../../user_interface/docks.rst:67
msgid "The \"A\" slider changes the color opacity (Alpha)."
msgstr ""

#: ../../user_interface/docks.rst:69
msgid ""
"The two color indicators show the main (fill) color and secondary (stroke) "
"color."
msgstr ""

#: ../../user_interface/docks.rst:71
msgid ""
"Clicking on either of them shows a dialog which gives similar controls as "
"the ones in the view, but more compact. And also allows picking colors from "
"the screen."
msgstr ""

#: ../../user_interface/docks.rst:73
msgid "The :guilabel:`Swap` button |swap-panels| swaps the two colors."
msgstr ""

#: ../../user_interface/docks.rst:75
msgid ""
"Finally, there's a textbox with a hexadecimal representation of the main "
"color."
msgstr ""

#: ../../user_interface/docks.rst:78
msgid "HSV"
msgstr "HSV"

#: ../../user_interface/docks.rst:85
msgid "HSV View"
msgstr "Vista HSV"

#: ../../user_interface/docks.rst:87
msgid "Colors selectors in the HSV space (Hue Saturation Value)."
msgstr ""

#: ../../user_interface/docks.rst:89 ../../user_interface/docks.rst:103
msgid ""
"The color can be adjusted with the color wheel or individual channel sliders."
msgstr ""

#: ../../user_interface/docks.rst:92
msgid "HSL"
msgstr "HSL"

#: ../../user_interface/docks.rst:99
msgid "HSL View"
msgstr "Vista HSL"

#: ../../user_interface/docks.rst:101
msgid "Colors selectors in the HSL space (Hue Saturation Lightness)."
msgstr ""

#: ../../user_interface/docks.rst:106
msgid "RGB"
msgstr "RGB"

#: ../../user_interface/docks.rst:113
msgid "RGB View"
msgstr "Vista RGB"

#: ../../user_interface/docks.rst:115
msgid "Colors selectors in the RGB space (Red Green Blue)."
msgstr ""

#: ../../user_interface/docks.rst:117 ../../user_interface/docks.rst:131
msgid "The color can be adjusted with the individual channel sliders."
msgstr ""

#: ../../user_interface/docks.rst:120
msgid "CMYK"
msgstr "CMYK"

#: ../../user_interface/docks.rst:127
msgid "CMYK View"
msgstr ""

#: ../../user_interface/docks.rst:129
msgid "Colors selectors in the CMYK space (Cyan Magenta Yellow Black)."
msgstr ""

#: ../../user_interface/docks.rst:134
msgid "Palette"
msgstr "Tavolozza"

#: ../../user_interface/docks.rst:141
msgid "Palette View"
msgstr ""

#: ../../user_interface/docks.rst:143
msgid "Here you can select, create, modify color palettes."
msgstr ""

#: ../../user_interface/docks.rst:145
msgid "Opening and saving supports Gimp Palette files (.gpl)."
msgstr ""

#: ../../user_interface/docks.rst:147
msgid "Clicking on one of the colors, selects it as the current color."
msgstr ""

#: ../../user_interface/docks.rst:152
msgid "Stroke"
msgstr "Tratto"

#: ../../user_interface/docks.rst:154
msgid "This view shows various stroke (outline) settings."
msgstr ""

#: ../../user_interface/docks.rst:157
msgid "Stroke Style"
msgstr "Stile del tratto"

#: ../../user_interface/docks.rst:164 ../../user_interface/docks.rst:184
#: ../../user_interface/docks.rst:198
msgid "Stroke Style Dock"
msgstr ""

#: ../../user_interface/docks.rst:166
msgid "At the bottom it shows a preview of the current settings."
msgstr ""

#: ../../user_interface/docks.rst:168
msgid "The \"Width\" spin box determines how thick the stroke is."
msgstr ""

#: ../../user_interface/docks.rst:170
msgid "The \"Cap\" buttons determine the style of the ends of the line."
msgstr ""

#: ../../user_interface/docks.rst:172
msgid "The \"Join\" buttons determine the style of sharp corners."
msgstr ""

#: ../../user_interface/docks.rst:174
msgid ""
"The spin box after the \"Join\" buttons determines how far a Miter join can "
"reach."
msgstr ""

#: ../../user_interface/docks.rst:177
msgid "Stroke Color"
msgstr ""

#: ../../user_interface/docks.rst:186
msgid ""
"This tab selects the stroke color in a similar way as to how the :ref:`Fill "
"View<fill_dock>` selects the fill color."
msgstr ""

#: ../../user_interface/docks.rst:191
msgid "Layers"
msgstr "Livelli"

#: ../../user_interface/docks.rst:200
msgid "This view shows layers and shapes."
msgstr ""

#: ../../user_interface/docks.rst:202
msgid ""
"From here you can rename them by double clicking on their name. You can hide "
"and lock them by clicking on the eye and padlock icons respectively."
msgstr ""

#: ../../user_interface/docks.rst:204
msgid ""
"You can also select a grouping color by clicking on the rectangle to thee "
"left. This has no effect other than changing how layers are shown in this "
"view to help organize them."
msgstr ""

#: ../../user_interface/docks.rst:207 ../../user_interface/docks.rst:214
#: ../../user_interface/docks.rst:621
msgid "Context Menu"
msgstr "Menu contestuale"

#: ../../user_interface/docks.rst:216
msgid ""
"Right clicking on an item will bring a context menu with quick actions for "
"that item."
msgstr ""

#: ../../user_interface/docks.rst:220
msgid "Timeline"
msgstr "Linea temporale"

#: ../../user_interface/docks.rst:227
msgid "Timeline Dock"
msgstr ""

#: ../../user_interface/docks.rst:229
msgid ""
"The timeline view allows to manage keyframes for the animatable properties "
"of the active object."
msgstr ""

#: ../../user_interface/docks.rst:232 ../../user_interface/docks.rst:239
msgid "Playback Buttons"
msgstr ""

#: ../../user_interface/docks.rst:241
msgid ""
"At the top of the Timeline view shows the current frame, and various "
"playback buttons."
msgstr ""

#: ../../user_interface/docks.rst:243
msgid ""
"The frame spin box shows the current frame, and allows jumping to a specific "
"frame by typing its number."
msgstr ""

#: ../../user_interface/docks.rst:245
msgid "|media-playback-start| Starts and pauses playback"
msgstr ""

#: ../../user_interface/docks.rst:247
msgid "|media-playlist-repeat| Toggles whether playback loops the animation"
msgstr ""

#: ../../user_interface/docks.rst:249
msgid "|go-first| Jumps to the first frame"
msgstr ""

#: ../../user_interface/docks.rst:251
msgid "|go-previous| Jumps to the previous frame"
msgstr ""

#: ../../user_interface/docks.rst:253
msgid "|go-next| Jumps to the next frame"
msgstr ""

#: ../../user_interface/docks.rst:255
msgid "|go-last| Jumps to the last frame"
msgstr ""

#: ../../user_interface/docks.rst:257
msgid ""
"|media-record| Record keyframes. When enabled, all changes made on the "
"canvas are added as keyframes."
msgstr ""

#: ../../user_interface/docks.rst:260 ../../user_interface/docks.rst:267
msgid "Composition Tabs"
msgstr ""

#: ../../user_interface/docks.rst:269
msgid ""
"If you have multiple compositions in the current document, they are shown in "
"a tab bar above the property list and timeline."
msgstr ""

#: ../../user_interface/docks.rst:271
msgid "Clicking on the tabs will switch to a different composition."
msgstr ""

#: ../../user_interface/docks.rst:274
msgid "Property and Object List"
msgstr ""

#: ../../user_interface/docks.rst:281
msgid "Property List"
msgstr ""

#: ../../user_interface/docks.rst:283
msgid ""
"On its left handside, the Timeline view has the list of objects and "
"properties."
msgstr ""

#: ../../user_interface/docks.rst:285
msgid ""
"For shapes and other objects, it shows their group color, visibility, lock "
"status, and name. All of those can be modified from the timeline itself."
msgstr ""

#: ../../user_interface/docks.rst:287
msgid ""
"Additionally, top-level layers can also have a parent, which is also shown "
"and edited from the timeline."
msgstr ""

#: ../../user_interface/docks.rst:289
msgid "Nested inside each object, you can see their children and properties."
msgstr ""

#: ../../user_interface/docks.rst:291
msgid "Properties show their name, current value, and animation status."
msgstr ""

#: ../../user_interface/docks.rst:294
msgid "The animation statuses are as follow:"
msgstr ""

#: ../../user_interface/docks.rst:296
msgid "|keyframe-not-animated| Not animated"
msgstr ""

#: ../../user_interface/docks.rst:298
msgid "|keyframe-key| Currently on a keyframe"
msgstr ""

#: ../../user_interface/docks.rst:300
msgid "|keyframe-tween| \"Tweening\" value, between keyframes"
msgstr ""

#: ../../user_interface/docks.rst:302
msgid ""
"|keyframe-mismatch| Animated value that has been moved without adding a "
"keyframe"
msgstr ""

#: ../../user_interface/docks.rst:305
msgid "Keyframe Timeline"
msgstr ""

#: ../../user_interface/docks.rst:307
msgid "The right handside of the Timeline view shows the actual timeline."
msgstr ""

#: ../../user_interface/docks.rst:310
msgid ""
"Pressing left/right arrow keys changes the current frame when this area is "
"focused."
msgstr ""

#: ../../user_interface/docks.rst:314 ../../user_interface/docks.rst:321
msgid "Time Bar"
msgstr ""

#: ../../user_interface/docks.rst:323
msgid ""
"The bar at the top highlights the current frame. Clicking or dragging with "
"the mouse will jump to the frame whose number is under the mouse cursor."
msgstr ""

#: ../../user_interface/docks.rst:327 ../../user_interface/docks.rst:334
msgid "Keyframe Area"
msgstr ""

#: ../../user_interface/docks.rst:336
msgid ""
"Under that, there are rows for each of the animatable properties, and icons "
"showing the keyframes for said properties."
msgstr ""

#: ../../user_interface/docks.rst:338
msgid ""
"The keyframes icons can be selected by clicking (To select multiple :kbd:"
"`Shift+Click` or :kbd:`Ctrl+Click`). and dragged with the mouse to change "
"their time."
msgstr ""

#: ../../user_interface/docks.rst:340
msgid ""
"The keyframe icons show different shapes and colors depending on the easing "
"transition of the keyframe before and after the icon."
msgstr ""

#: ../../user_interface/docks.rst:342
msgid ""
"|keyframe-linear| Linear, values change from the start to the end of the "
"keyframe at a constant rate"
msgstr ""

#: ../../user_interface/docks.rst:344
msgid ""
"|keyframe-ease| Smooth Easing, values change more slowly at the start or end "
"of the keyframe"
msgstr ""

#: ../../user_interface/docks.rst:346
msgid ""
"|keyframe-hold| Hold, the value remains constant until the next keyframe"
msgstr ""

#: ../../user_interface/docks.rst:348
msgid ""
"|keyframe-custom| Custom curve, defined with the dialog described in [Custom "
"Easing Dialog](#custom-easing-dialog)"
msgstr ""

#: ../../user_interface/docks.rst:350
msgid ""
":kbd:`Ctrl+Mouse Wheel` will zoom the area with respect to time, :kbd:`Mouse "
"Wheel` by itself moves left or right. To scroll up and down either use the "
"scrollbar to the side or use the :kbd:`Mouse Wheel` on the property list."
msgstr ""

#: ../../user_interface/docks.rst:352
msgid ""
"Layers will be displayed as rectangles that spans between the first and last "
"frames of that layer:"
msgstr ""

#: ../../user_interface/docks.rst:359
msgid "Keyframe Area with Layers"
msgstr ""

#: ../../user_interface/docks.rst:362
msgid "Context Menus"
msgstr "Menu contestuali"

#: ../../user_interface/docks.rst:364
msgid ""
"Right clicking on the row of a property, will show a context menu with the "
"name of the property and the ability to add a keyframe:"
msgstr ""

#: ../../user_interface/docks.rst:371
msgid "Property Menu"
msgstr ""

#: ../../user_interface/docks.rst:373
msgid ""
"Clicking on \"Add Keyframe\" will add a keyframe at the current frame with "
"the current value of the property."
msgstr ""

#: ../../user_interface/docks.rst:375
msgid ""
"Right clicking on an existing keyframe icon, will show a menu with options "
"relating to that keyframe:"
msgstr ""

#: ../../user_interface/docks.rst:382
msgid "Keyframe Menu"
msgstr ""

#: ../../user_interface/docks.rst:384
msgid ""
"Clicking on \"Custom...\" for the incoming or outgoing transitions will show "
"the [Custom Easing Dialog](#custom-easing-dialog)."
msgstr ""

#: ../../user_interface/docks.rst:387 ../../user_interface/docks.rst:394
msgid "Custom Easing Dialog"
msgstr ""

#: ../../user_interface/docks.rst:396
msgid "This dialog gives full freedom to specify easing curves."
msgstr ""

#: ../../user_interface/docks.rst:398
msgid ""
"The curve represents how the value changes between two keyframes: the "
"horizontal axis represents the time between the two keyframes and the "
"vertical axis is the interpolation factor between the two values."
msgstr ""

#: ../../user_interface/docks.rst:402
msgid ""
"To choose a preset transition for one of the two ends, you can select a "
"value from the dropdowns."
msgstr ""

#: ../../user_interface/docks.rst:406
msgid "Align"
msgstr "Allinea"

#: ../../user_interface/docks.rst:413
msgid "Align View"
msgstr ""

#: ../../user_interface/docks.rst:415
msgid ""
"This view shows alignment options, the same actions are available in the "
"[Align Menu](menus.md#align)."
msgstr ""

#: ../../user_interface/docks.rst:417
msgid ""
"The dropdown selects whether to align based on the selection bounding box or "
"the canvas, the buttons are the various alignment options."
msgstr ""

#: ../../user_interface/docks.rst:422
msgid "Properties"
msgstr "Proprietà"

#: ../../user_interface/docks.rst:429
msgid "Property View"
msgstr ""

#: ../../user_interface/docks.rst:431
msgid ""
"The Properties view allows the user to view or modify any property of the "
"active object."
msgstr ""

#: ../../user_interface/docks.rst:433
msgid "To edit a property double click on its value."
msgstr ""

#: ../../user_interface/docks.rst:435
msgid ""
"Animatable properties are highlighted as explained in [Property List]"
"(#property-list)."
msgstr ""

#: ../../user_interface/docks.rst:439
msgid "Undo History"
msgstr ""

#: ../../user_interface/docks.rst:446
msgid "Undo History View"
msgstr ""

#: ../../user_interface/docks.rst:448
msgid ""
"This view allows you to see the action history for the current document, "
"clicking on the listed actions will revert the document to that action."
msgstr ""

#: ../../user_interface/docks.rst:451
msgid "Useful for when you want to undo or redo several actions at once."
msgstr ""

#: ../../user_interface/docks.rst:453
msgid ""
"A little icon will be shown next to the action corresponding to the last "
"time you saved the document."
msgstr ""

#: ../../user_interface/docks.rst:457
msgid "Gradients"
msgstr "Sfumature"

#: ../../user_interface/docks.rst:464 ../../user_interface/docks.rst:511
#: ../../user_interface/docks.rst:523
msgid "Gradients View"
msgstr ""

#: ../../user_interface/docks.rst:466
msgid "This view shows the gradients used in the document."
msgstr ""

#: ../../user_interface/docks.rst:468
msgid ""
"It's hidden by default, to show it use the :menuselection:`View --> Views --"
"> Gradients` menu action."
msgstr ""

#: ../../user_interface/docks.rst:470
msgid ""
"At the top of the view is the list of gradients, showing a preview, its "
"name, and the number of shapes using it."
msgstr ""

#: ../../user_interface/docks.rst:473
msgid "Below that, you have a few buttons:"
msgstr ""

#: ../../user_interface/docks.rst:475
msgid "|list-add| Adds a new gradient based on the current colors."
msgstr ""

#: ../../user_interface/docks.rst:476
msgid "|folder| Adds a new gradients from a list of presets."
msgstr ""

#: ../../user_interface/docks.rst:477
msgid "|list-remove| Removes the selected gradient."
msgstr ""

#: ../../user_interface/docks.rst:479
msgid "Then buttons to apply the selected gradient for fill or stroke:"
msgstr ""

#: ../../user_interface/docks.rst:481
msgid "|paint-gradient-linear| for a linear gradient."
msgstr ""

#: ../../user_interface/docks.rst:483
msgid "|paint-gradient-radial| for a radial gradient."
msgstr ""

#: ../../user_interface/docks.rst:485
msgid "Click on the checked button to remove the gradient from an object."
msgstr ""

#: ../../user_interface/docks.rst:488
msgid "Presets"
msgstr "Preimpostazioni"

#: ../../user_interface/docks.rst:495
msgid "Gradients Presets"
msgstr ""

#: ../../user_interface/docks.rst:497
msgid ""
"This menu shows a drop down with the available presets, based on https://"
"webgradients.com/."
msgstr ""

#: ../../user_interface/docks.rst:500
msgid "Editing from the List"
msgstr ""

#: ../../user_interface/docks.rst:502
msgid ""
"You can rename gradients by double-clicking on their name and typing a new "
"name."
msgstr ""

#: ../../user_interface/docks.rst:504
msgid "Similarly, you can double click on the preview to make it editable:"
msgstr ""

#: ../../user_interface/docks.rst:513
msgid ""
"While in this mode, you can drag the lines representing the gradient stops "
"to move them, drag them off the rectangle to remove them, and double click "
"to show a dialog to set the color."
msgstr ""

#: ../../user_interface/docks.rst:516
msgid "Right-clicking shows this menu:"
msgstr ""

#: ../../user_interface/docks.rst:527
msgid "Editing from the Canvas"
msgstr ""

#: ../../user_interface/docks.rst:529
msgid ""
"With the [edit tool](tools.md#edit-tool) active, the gradient controls will "
"appear on the canvas."
msgstr ""

#: ../../user_interface/docks.rst:531
msgid ""
"Dragging the end points will change the extent of the gradient and dragging "
"the smaller handles will change the offset of the corresponding gradient "
"stop."
msgstr ""

#: ../../user_interface/docks.rst:534
msgid "For a Linear gradient:"
msgstr ""

#: ../../user_interface/docks.rst:541
msgid "Linear Gradient"
msgstr ""

#: ../../user_interface/docks.rst:543
msgid "For a radial gradient:"
msgstr ""

#: ../../user_interface/docks.rst:550
msgid "Radial Gradient"
msgstr "Gradiente radiale"

#: ../../user_interface/docks.rst:552
msgid ""
"When you have a radial gradient, you can shift click on the start handle to "
"reveal a new X-shaped handle that controls the highlight position for the "
"gradient:"
msgstr ""

#: ../../user_interface/docks.rst:560
msgid "Radial Highlight"
msgstr ""

#: ../../user_interface/docks.rst:562
msgid "To hide it again, shift-click on it."
msgstr ""

#: ../../user_interface/docks.rst:566
msgid "Swatch"
msgstr "Campione"

#: ../../user_interface/docks.rst:573
msgid "Swatch View"
msgstr ""

#: ../../user_interface/docks.rst:575
msgid "This view shows the colors in the document swatch."
msgstr ""

#: ../../user_interface/docks.rst:577
msgid ""
"The document swatch is a palette specific to the open document. When an "
"object is assigned a color from the document swatch, it gets linked to it. "
"Modifying the color in the swatch will reflect the change in all linked "
"objects, so you can recolor multiple objects at once."
msgstr ""

#: ../../user_interface/docks.rst:579
msgid ""
"The view shows the palette for the document swatch, which each color in its "
"own rectangle. There is an extra rectangle that's used to unlink the "
"selected object from the swatch (the object will retain its color but "
"modifying the swatch will no longer affect that object)."
msgstr ""

#: ../../user_interface/docks.rst:582
msgid "These are the buttons at the bottom:"
msgstr ""

#: ../../user_interface/docks.rst:585
msgid ""
"|open-menu-symbolic| Clicking and holding on this button will show [a menu "
"with extra options](#swatch-extra-options)."
msgstr ""

#: ../../user_interface/docks.rst:587
msgid ""
"|list-add| Adds the current fill color to the swatch. If an object is "
"selected, its fill color will be linked to the swatch."
msgstr ""

#: ../../user_interface/docks.rst:589
msgid "|list-remove| Removes the last clicked color from the swatch"
msgstr ""

#: ../../user_interface/docks.rst:592
msgid "Swatch Extra Options"
msgstr ""

#: ../../user_interface/docks.rst:599
msgid "Menu Extra"
msgstr ""

#: ../../user_interface/docks.rst:601
msgid ""
"|document-export| :guilabel:`Generate` Pulls the colors off the open "
"document and link all objects to the swatch"
msgstr ""

#: ../../user_interface/docks.rst:603
msgid ""
"|document-open| :guilabel:`From Palette...` Shows a [dialog](#from-palette) "
"with option to populate the swatch from an existing palette."
msgstr ""

#: ../../user_interface/docks.rst:605
msgid ""
"|document-save| :guilabel:`Save Palette` Saves the swatch and it will show "
"up as a [palette in the Fill and Stroke views](#palette)."
msgstr ""

#: ../../user_interface/docks.rst:608
msgid "Mouse interactions"
msgstr "Interazioni con il mouse"

#: ../../user_interface/docks.rst:610
msgid ""
"Left-clicking on one of the rectangles of the swatch will link the fill "
"color of the selected object (or unlink if you click the rectangle marked "
"with an X)."
msgstr ""

#: ../../user_interface/docks.rst:612
msgid ""
"Holding Shift when you click will affect the stroke color instead of the "
"fill color."
msgstr ""

#: ../../user_interface/docks.rst:614
msgid "Right clicking will show the following context menu:"
msgstr ""

#: ../../user_interface/docks.rst:623
msgid "At the top of the menu you see the name of the color"
msgstr ""

#: ../../user_interface/docks.rst:625
msgid ""
"|edit-rename| :guilabel:`Rename...` Shows a dialog where you can change the "
"name of the color."
msgstr ""

#: ../../user_interface/docks.rst:628
msgid ""
"|color-management| :guilabel:`Edit Color...` Shows a dialog where you can "
"change the color"
msgstr ""

#: ../../user_interface/docks.rst:631
msgid ""
"|list-remove| :guilabel:`Remove` Will remove the color from the swatch. This "
"will have no visual changes, but all objects previously linked to this color "
"will be unlinked."
msgstr ""

#: ../../user_interface/docks.rst:636
msgid ""
"|edit-duplicate| :guilabel:`Duplicate` Will add a new identical color to the "
"swatch."
msgstr ""

#: ../../user_interface/docks.rst:639
msgid "|list-remove| :guilabel:`Set as fill` Does the same as clicking"
msgstr ""

#: ../../user_interface/docks.rst:642
msgid ""
"|format-fill-color| :guilabel:`Set as stroke` Does the same as shift-clicking"
msgstr ""

#: ../../user_interface/docks.rst:645
msgid ""
"|format-stroke-color| :guilabel:`Link shapes with matching colors` Searches "
"the document for all shapes with the same color as the one in the rectangle "
"you clicked, and links them to the swatch."
msgstr ""

#: ../../user_interface/docks.rst:649
msgid "From Palette"
msgstr ""

#: ../../user_interface/docks.rst:656
msgid "From Palette Dialog"
msgstr ""

#: ../../user_interface/docks.rst:658
msgid ""
"At the top it shows a dropdown with the palettes currently loaded by "
"glaxnimate. The selected item will define the colors used by the document "
"swatch."
msgstr ""

#: ../../user_interface/docks.rst:660
msgid ""
"To add more palettes, you can do so in the [Palette section of the fill View]"
"(#palette)."
msgstr ""

#: ../../user_interface/docks.rst:662
msgid ""
":guilabel:`Overwrite on save` means that clicking :guilabel:`Save Palette` "
"for the document swatch will overwrite the selected palette."
msgstr ""

#: ../../user_interface/docks.rst:664
msgid ""
":guilabel:`Link shapes matching colors` When checked, Glaxnimate will scan "
"the document for objects with colors in the palette and link them to the "
"swatch."
msgstr ""

#: ../../user_interface/docks.rst:666
msgid ""
":guilabel:`Remove existing colors` If checked, any existing colors in the "
"document swatch will be removed before adding new ones from the palette. "
"Note that this means all objects will become unlinked."
msgstr ""

#: ../../user_interface/docks.rst:672
msgid "Assets"
msgstr ""

#: ../../user_interface/docks.rst:679
msgid "Assets View"
msgstr ""

#: ../../user_interface/docks.rst:681
msgid "This view shows a list of various assets used by the current document."
msgstr ""

#: ../../user_interface/docks.rst:685 ../../user_interface/docks.rst:692
msgid "Script Console"
msgstr "Console degli script"

#: ../../user_interface/docks.rst:694
msgid ""
"This view (hidden by default) allows you to inspect and modify the open "
"document with Python."
msgstr ""

#: ../../user_interface/docks.rst:696
msgid "See [Scripting](/contributing/scripting/index.md) for details."
msgstr ""

#: ../../user_interface/docks.rst:700 ../../user_interface/docks.rst:707
msgid "Snippets"
msgstr "Frammenti"

#: ../../user_interface/docks.rst:709
msgid ""
"Here you can see \"snippets\", small python files you can quickly edit and "
"execute."
msgstr ""

#: ../../user_interface/docks.rst:711
msgid ""
"Useful for testing new scripts when making plugins and to automate certain "
"tasks without having to provide all the metadata required by plugins."
msgstr ""

#: ../../user_interface/docks.rst:713
msgid "Editing a snippet will open your system text editor."
msgstr ""

#: ../../user_interface/docks.rst:715
msgid ""
"When you run a snippet, it's equivalent to running that code in the console."
msgstr ""

#: ../../user_interface/docks.rst:719
msgid "Logs"
msgstr "Registri"

#: ../../user_interface/docks.rst:726
msgid "Log View"
msgstr "Vista dei registri"

#: ../../user_interface/docks.rst:728
msgid "This view (hidden by default) shows internal reporting, errors, etc."
msgstr ""
